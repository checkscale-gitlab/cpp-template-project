# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

macro(include_module path)
  include(${CMAKE_CURRENT_LIST_DIR}/${path}.cmake)
endmacro()

# Pre-setup

include_module(build/PreventInSourceBuilds)
include_module(build/StandardProjectSettings)

# Conan

include_module(Conan)
run_conan()

# Build options

include_module(build/Cache)
include_module(build/Linker)

# Analyzers

include_module(Analyzers)

# Target utils

include_module(TargetInit)
include_module(TargetTest)
