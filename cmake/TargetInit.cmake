# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include(cmake/build/CompilerWarnings.cmake)
include(cmake/build/Sanitizers.cmake)
include(cmake/build/TimeTrace.cmake)

function(target_init _project)

  set_target_properties(${_project}
    PROPERTIES
      CXX_STANDARD 17
      CXX_STANDARD_REQUIRED YES
      CXX_EXTENSIONS NO
  )
  set_target_properties(${_project}
    PROPERTIES
      CXX_VISIBILITY_PRESET hidden
      VISIBILITY_INLINES_HIDDEN 1
  )

  # Standard compiler warnings
  set_project_warnings(${_project})

  # Sanitizer options if supported by compiler
  enable_sanitizers(${_project})

  # Time trace options if supported by compiler
  enable_time_trace(${_project})

endfunction()
