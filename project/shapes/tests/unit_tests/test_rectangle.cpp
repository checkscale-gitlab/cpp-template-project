/*
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/rectangle_impl.hpp"

#include <boost/test/unit_test.hpp>

/*
Test plan:

Done    1. Constructor
Done        1.1. Top-left right-bottom
Done        1.2. Top-left width-height
Done        1.3. Top-left right bottom (wrong)
Done        1.4. Top-left width-height (wrong)
Done    2. Access all points
Done    3. Access width-height
Done    4. Get rectangle perimeter
Done    5. Get rectangle area
Done    6. Contains point
Done    7. Intersects rectangle
Done    8. Covers rectangle
*/

namespace Ctp {
namespace Shapes {

BOOST_AUTO_TEST_SUITE( RectangleTests )

BOOST_AUTO_TEST_CASE( Constructor_TopLeftRightBottom_1_1 )
{
	RectangleImpl r{ { 0.0, 5.0 }, { 3.0, 1.0 } };

	BOOST_CHECK_EQUAL( r.getTopLeft(), Common::Point( 0.0, 5.0 ) );
	BOOST_CHECK_EQUAL( r.getBottomRight(), Common::Point( 3.0, 1.0 ) );
}

BOOST_AUTO_TEST_CASE( Constructor_TopLeftRightBottom_Wrong_1_2 )
{
    using Point = Common::Point;

    BOOST_CHECK_THROW(
        RectangleImpl( Point( 0.0, 5.0 ), Point( -2.0, 3.0 ) ),
        std::logic_error
    );
    BOOST_CHECK_THROW(
        RectangleImpl( Point( 0.0, 5.0 ), Point( 2.0, 6.0 ) ),
        std::logic_error
    );
}

BOOST_AUTO_TEST_CASE( Constructor_TopLeftWidthHeight_1_3 )
{
	RectangleImpl r{ { 0.0, 5.0 }, 2.0, 4.0 };

	BOOST_CHECK_EQUAL( r.getTopLeft(), Common::Point( 0.0, 5.0 ) );
	BOOST_CHECK_EQUAL( r.getBottomRight(), Common::Point( 2.0, 1.0 ) );
}

BOOST_AUTO_TEST_CASE( Constructor_TopLeftWidthHeight_Wrong_1_4 )
{
    using Point = Common::Point;

    BOOST_CHECK_THROW(
        RectangleImpl( Point( 0.0, 5.0 ), -2.0, 3.0 ),
        std::logic_error
    );
    BOOST_CHECK_THROW(
        RectangleImpl( Point( 0.0, 5.0 ), 2.0, -1.0 ),
        std::logic_error
    );
}

BOOST_AUTO_TEST_CASE( AccessAllPoints_2 )
{
	RectangleImpl r1{ { 0.0, 5.0 }, { 3.0, 1.0 } };
    using Point = Common::Point;

	BOOST_CHECK_EQUAL( r1.getTopLeft(), Point( 0.0, 5.0 ) );
	BOOST_CHECK_EQUAL( r1.getTopRight(), Point( 3.0, 5.0 ) );
	BOOST_CHECK_EQUAL( r1.getBottomLeft(), Point( 0.0, 1.0 ) );
	BOOST_CHECK_EQUAL( r1.getBottomRight(), Point( 3.0, 1.0 ) );

	RectangleImpl r2{ { 0.0, 5.0 }, 2.0, 3.0 };

	BOOST_CHECK_EQUAL( r2.getTopLeft(), Point( 0.0, 5.0 ) );
	BOOST_CHECK_EQUAL( r2.getTopRight(), Point( 2.0, 5.0 ) );
	BOOST_CHECK_EQUAL( r2.getBottomLeft(), Point( 0.0, 2.0 ) );
	BOOST_CHECK_EQUAL( r2.getBottomRight(), Point( 2.0, 2.0 ) );
}

BOOST_AUTO_TEST_CASE( AccessWidthHeight_3 )
{
	RectangleImpl r1{ { 0.0, 5.0 }, { 3.0, 1.0 } };

	BOOST_CHECK( Common::isEqual( r1.getWidth(), 3.0 ) );
	BOOST_CHECK( Common::isEqual( r1.getHeight(), 4.0 ) );

	RectangleImpl r2{ { 0.0, 5.0 }, 2.0, 3.0 };

	BOOST_CHECK( Common::isEqual( r2.getWidth(), 2.0 ) );
	BOOST_CHECK( Common::isEqual( r2.getHeight(), 3.0 ) );
}

BOOST_AUTO_TEST_CASE( GetRectanglePerimeter_4 )
{
	RectangleImpl r{ { 0.0, 5.0 }, { 3.0, 1.0 } };
	BOOST_CHECK( Common::isEqual( r.getPerimeter(), 14.0 ) );
}

BOOST_AUTO_TEST_CASE( GetRectangleArea_5 )
{
	RectangleImpl r{ { 0.0, 5.0 }, { 3.0, 1.0 } };
	BOOST_CHECK( Common::isEqual( r.getArea(), 12.0 ) );
}

BOOST_AUTO_TEST_CASE( ContainsPoint_6 )
{
	RectangleImpl r{ { 0.0, 5.0 }, { 3.0, 1.0 } };

	BOOST_CHECK( r.contains( Common::Point( 1.5, 3.0 ) ) );
	BOOST_CHECK( r.contains( Common::Point( 0.0, 5.0 ) ) );
	BOOST_CHECK( r.contains( Common::Point( 3.0, 3.0 ) ) );

	BOOST_CHECK( !r.contains( Common::Point( 0.0, 0.0 ) ) );
	BOOST_CHECK( !r.contains( Common::Point( 4.0, 3.0 ) ) );
}

BOOST_AUTO_TEST_CASE( IntersectsRectangle_7 )
{
	RectangleImpl r1{ {  0.0, 5.0 }, { 3.0, 1.0 } };
	RectangleImpl r2{ {  1.0, 3.0 }, { 5.0, 0.0 } };
	RectangleImpl r3{ {  0.0, 0.0 }, { 5.0, -2.0 } };
	RectangleImpl r4{ {  4.0, 3.0 }, { 5.0, 2.0 } };

	BOOST_CHECK( r1.intersects( r2 ) );
	BOOST_CHECK( !r1.intersects( r3 ) );
	BOOST_CHECK( !r1.intersects( r4 ) );
}

BOOST_AUTO_TEST_CASE( CoversRectangle_8 )
{
	RectangleImpl r1{ { 0.0, 5.0 }, { 3.0, 1.0 } };
	RectangleImpl r2{ { 1.0, 4.0 }, { 2.0, 2.0 } };
	RectangleImpl r3{ { 1.0, 4.0 }, { 3.5, 2.0 } };
	RectangleImpl r4{ { 1.0, 4.0 }, { 2.0, 0.0 } };

	BOOST_CHECK( r1.covers( r2 ) );
	BOOST_CHECK( !r1.covers( r3 ) );
	BOOST_CHECK( !r1.covers( r4 ) );
}

BOOST_AUTO_TEST_SUITE_END() // RectangleTests

} // namespace Shapes
} // namespace Ctp
