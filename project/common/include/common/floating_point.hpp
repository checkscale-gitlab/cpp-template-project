/*
 * Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
 *
 * Cpp Template Project: A template CMake project to get you started with
 * C++ and tooling.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CTP_COMMON_FLOATINGPOINT_HPP
#define CTP_COMMON_FLOATINGPOINT_HPP

#include <cmath>
#include <limits>

namespace Ctp {
namespace Common {

/**
 * @brief Checks whether @c _lhs is equal to @c _rhs.
 *
 * @return @c true if @c _lhs is approximately equal to @c _rhs, @c false otherwise.
 */
[[nodiscard]]
constexpr bool
isEqual ( double _lhs, double _rhs ) noexcept
{
    constexpr auto eps = std::numeric_limits< double >::epsilon();
    return std::fabs( _lhs - _rhs ) < eps;
}

} // namespace Common
} // namespace Ctp

#endif // CTP_COMMON_FLOATINGPOINT_HPP
